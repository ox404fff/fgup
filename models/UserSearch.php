<?php

namespace app\models;
use yii\data\ActiveDataProvider;

/**
 * Class UserSearch
 * @package app\models
 */
class UserSearch extends User
{

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'number'],
            ['name', 'string', 'length' => [2, 256]],
            ['country_id', 'app\components\validators\CountryValidator'],
        ];
    }

    /**
     * Get data provider for render users list
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->with('country');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => [
                    'id'=>SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->name)) {
            $query->where('name ILIKE :name || \'%\'', [':name' => $this->name]);
        }

        if (!empty($this->id)) {
            $query->where('id = :id', [':id' => $this->id]);
        }

        return $dataProvider;
    }
}
