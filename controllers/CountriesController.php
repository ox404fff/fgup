<?php

namespace app\controllers;

use app\components\helpers\StringHelper;
use app\models\DictionaryCountry;
use Symfony\Component\CssSelector\Parser\Handler\StringHandler;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class CountriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Select countries for autocomplete.
     * @param string $term
     * @return string
     */
    public function actionAutoComplete($term)
    {
        if (is_numeric($term)) {
            $countries = [DictionaryCountry::findOne(['id' => $term])];
        } else {
            $countries = DictionaryCountry::findByName($term);
        }

        $result = [];
        foreach ($countries as $country) {
            $result[] = [
                'id'    => $country->id,
                'label' => StringHelper::ucFirst($country->name),
            ];
        }

        return $result;
    }

}
