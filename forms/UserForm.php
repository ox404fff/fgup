<?php
namespace app\forms;
use app\models\DictionaryCountry;
use app\models\User;
use yii\helpers\ArrayHelper;

/**
 *
 * Class UserFrom
 * @package app\models
 */
class UserForm extends User
{

    /**
     * Номер телефона, указанный пользвателем
     *
     * @var
     */
    public $phone_string;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [

            [['phone_string'], 'required'],
            [['phone_string'], 'trim'],

            ['phone_string', 'string', 'length' => [2, 20]],

            ['country_id', 'app\components\validators\CountryValidator'],

            ['phone_string', 'app\components\validators\PhoneFormatValidator',
                'countryIdAttribute'        => 'country_id',
                'phoneCountryCodeAttribute' => 'phone_country_code',
            ],

            ['phone_string', 'filter', 'filter' => [$this, 'parsePhone']],
        ]);
    }


    public function attributeLabels()
    {
        return [
            'name'         => \Yii::t('app', 'ФИО'),
            'phone_string' => \Yii::t('app', 'Телефон'),
            'country_id'   => \Yii::t('app', 'Страна'),
        ];
    }

    public function parsePhone($value)
    {
        $country = DictionaryCountry::getCountryByIdSC($this->country_id);

        if (empty($country)) {
            return null;
        }

        if (!preg_match($country->phone_mask, $this->phone_string, $matches)) {
            throw new \Exception('Что - то сломалось, обратитесь к администратору сайта');
        }

        $this->phone_country_code = $matches[1];
        $this->phone_operator = $matches[2];
        $this->phone_number = $matches[3];

        return $value;
    }

    /**
     * Fetch data from database
     *
     * @param $id
     * @return static
     */
    public static function loadData($id)
    {
        $model = self::findOne(['id' => $id]);

        $model->phone_string = $model->getPhone();

        return $model;
    }
}
