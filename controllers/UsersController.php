<?php

namespace app\controllers;

use app\forms\UserForm;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\HttpException;

class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays users list.
     *
     * @return string
     */
    public function actionIndex()
    {
        $userSearch = new UserSearch();

        $dataProvider = $userSearch->search(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'userSearch'   => $userSearch,
        ]);
    }


    /**
     * Create user action
     *
     * @return string
     * @throws \Exception
     */
    public function actionCreate()
    {
        return $this->_form();
    }


    /**
     * Update users
     *
     * @param null $id
     * @throws \Exception
     * @return string
     */
    public function actionUpdate($id)
    {
        return $this->_form($id);
    }

    /**
     * Edit users form
     *
     * @param $id
     * @return string
     * @throws \Exception
     */
    private function _form($id = null)
    {
        if (!is_null($id)) {
            $formModel = UserForm::loadData($id);
        } else {
            $formModel = new UserForm();
        }

        if (empty($formModel)) {
            throw new HttpException(404, 'User not found');
        }

        $statusSaved = false;
        if ($formModel->load(\Yii::$app->request->post()) && $formModel->validate()) {

            if ($formModel->save()) {
                $statusSaved = true;
                if ($formModel->getIsNewRecord()) {
                    $formModel = new UserForm();
                }
            } else {
                throw new \Exception('ЧТо - то сломалось, попробуйте перезагрузить страницу');
            }

        }

        return $this->render('form', [
            'formModel'   => $formModel,
            'statusSaved' => $statusSaved,
        ]);
    }

}
