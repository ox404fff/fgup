<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \keygenqt\autocompleteAjax\AutocompleteAjax;

/**
 * @var \app\forms\UserForm $formModel
 * @var bool $statusSaved
 */
?>
<?php $form = ActiveForm::begin(); ?>
    <? if ($statusSaved): ?>
    <div class="alert alert-success" role="alert">
        <? if ($formModel->getIsNewRecord()): ?>
            <?= \Yii::t('app', 'Пользователь успешно внесен в базу') ?>
        <? else: ?>
            <?= \Yii::t('app', 'Пользователь успешно сохранен') ?>
        <? endif ?>
    </div>
    <? endif ?>
<?= $form->field($formModel, 'phone_string') ?>

<?= $form->field($formModel, 'name') ?>

<?= $form->field($formModel, 'country_id')->widget(AutocompleteAjax::class, [
    'multiple' => false,
    'url' => [\yii\helpers\Url::to(['countries/auto-complete'])],
    'options' => ['placeholder' => 'Укажите страну']
]) ?>

    <div class="form-group">
        <?= Html::a('Назад', \yii\helpers\Url::to('/'), ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton(\Yii::t('app', $formModel->getIsNewRecord() ? 'Создать' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>