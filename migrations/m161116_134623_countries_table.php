<?php

use yii\db\Migration;

class m161116_134623_countries_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('dictionary_country', [
            'id'                => 'pk',
            'name'              => 'TEXT NOT NULL',
            'phone_mask'        => 'TEXT NOT NULL',
            'phone_placeholder' => 'TEXT NOT NULL',
            'phone_code'        => 'varchar(8)',
        ]);

        $this->createIndex('name_idx', 'dictionary_country', 'name');
    }

    public function safeDown()
    {
        $this->dropTable('dictionary_country');

        return true;
    }
}
