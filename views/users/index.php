<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $userSearch \app\models\UserSearch
 */

$this->title = 'Электронные торги и безопасность';
?>
<div class="site-index">
    <?php
    echo \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $userSearch,
        'columns' => [
            [
                'label' => \Yii::t('app', '#'),
                'attribute'  => 'id',
                'class' => 'yii\grid\DataColumn',
                'options' => [
                    'width' => '70px'
                ],
                'value' => function (\app\models\User $data) {
                    return $data->id;
                },
            ],
            [
                'label' => \Yii::t('app', 'ФИО'),
                'attribute'  => 'name',
                'class' => 'yii\grid\DataColumn',
                'value' => function (\app\models\User $data) {
                    return $data->name;
                },
            ],
            [
                'header' => \Yii::t('app', 'Страна'),
                'class' => 'yii\grid\DataColumn',
                'value' => function (\app\models\User $data) {
                    if (!empty($data->country)) {
                        return $data->country->name;
                    } else {
                        return \Yii::t('app', 'Удалена');
                    }
                },
            ],
            [
                'header' => \Yii::t('app', 'Номер телефона'),
                'class' => 'yii\grid\DataColumn',
                'value' => function (\app\models\User $data) {
                    return $data->getPhone();
                },
            ],
            [
                'class'   => 'yii\grid\ActionColumn',
                'buttons' => [
                    'delete' => function () { return ''; },
                    'view' => function () { return ''; },
                ]
            ],
        ],
    ]);

    ?>
</div>
