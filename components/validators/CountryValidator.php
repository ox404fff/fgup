<?php

namespace app\components\validators;
use app\models\DictionaryCountry;
use yii\validators\Validator;

/**
 * Check is country id exists in database
 */
class CountryValidator extends Validator
{


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }


    /**
     * Get is country exists
     *
     * @param $id
     * @return bool
     */
    protected function isCountryExists($id)
    {
        return (bool) $this->getCountry($id);
    }


    /**
     * Check is country id exists in database
     *
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $valid = $this->isCountryExists($value);

        if (!$valid) {
            $this->message = \Yii::t('app', 'Что - то сломалось, попробуйте перезагрузить старницу.');
        }

        return $valid ? null : [$this->message, []];
    }


    /**
     * Find country by id
     *
     * @param $id
     * @return DictionaryCountry|null
     */
    protected function getCountry($id)
    {
        return DictionaryCountry::getCountryByIdSC($id);
    }
}