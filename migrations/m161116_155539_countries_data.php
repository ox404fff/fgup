<?php

use yii\db\Migration;
use \app\models\DictionaryCountry;

class m161116_155539_countries_data extends Migration
{
    public function up()
    {
        $country = new DictionaryCountry();
        $country->name = 'россия';
        $country->phone_code = '7';
        $country->phone_mask = '/^\+(\d{1})-\((\d{3})\)-(\d{3}-\d{2}-\d{2})$/';
        $country->phone_placeholder = '+7-(900)-(111)-11-11';
        $country->save();

        $country = new DictionaryCountry();
        $country->name = 'украина';
        $country->phone_code = '38';
        $country->phone_mask = '/^\+(\d{3})-\((\d{3})\)-(\d{3}-\d{2}-\d{2})$/';
        $country->phone_placeholder = '+38-(050)-(111)-11-11';
        $country->save();

        $country = new DictionaryCountry();
        $country->name = 'уганда';
        $country->phone_code = '256';
        $country->phone_mask = '/^\+(\d{3})-\((\d{3})\)-(\d{3}-\d{2}-\d{2})$/';
        $country->phone_placeholder = '+256-(050)-(111)-11-11';
        $country->save();

        $country = new DictionaryCountry();
        $country->name = 'белоруссия';
        $country->phone_code = '375';
        $country->phone_mask = '/^\+(\d{3})-\((\d{3})\)-(\d{3}-\d{2}-\d{2})$/';
        $country->phone_placeholder = '+375-(050)-(111)-11-11';

        $country->save();
    }

    public function down()
    {
        $this->truncateTable('dictionary_country');
    }

}
