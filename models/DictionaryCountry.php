<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $name
 * @property string $phone_code
 * @property string $phone_mask
 * @property string $phone_placeholder
 *
 * Class DictionaryCountry
 * @package app\models
 */
class DictionaryCountry extends ActiveRecord
{

    /**
     * Static cache for countries
     *
     * @var array
     */
    protected static $countryStaticCache = [];

    /**
     * @return string the name of the table
     */
    public static function tableName()
    {
        return 'dictionary_country';
    }


    /**
     * Find country by name
     *
     * @param $name
     * @param $limit
     * @return DictionaryCountry[]
     */
    public static function findByName($name, $limit = 10)
    {
        $condition = self::find()
            ->where('name LIKE :name || \'%\'', [':name' => mb_strtolower($name)])
            ->limit($limit);

        return $condition->all();
    }


    /**
     * Верент допустимые коды стран
     *
     * @return array
     */
    public function getPhoneCode()
    {
        return [$this->phone_code];
    }


    /**
     * Find country by id with static caching
     *
     * @param $id
     * @return DictionaryCountry|null
     */
    public static function getCountryByIdSC($id)
    {
        if (array_key_exists($id, self::$countryStaticCache)) {
            return self::$countryStaticCache[$id];
        }

        self::$countryStaticCache[$id] = DictionaryCountry::findOne(['id' => (int) $id]);

        return self::$countryStaticCache[$id];
    }

}
