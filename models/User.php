<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $phone_operator
 * @property string $phone_number
 * @property string $phone_country_code
 *
 * @property DictionaryCountry $country
 *
 * Class User
 * @package app\models
 */
class User extends ActiveRecord
{


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [['name'], 'trim'],

            ['name', 'string', 'length' => [6, 256]],

            ['country_id', 'app\components\validators\CountryValidator'],
        ];
    }


    /**
     * related country
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(DictionaryCountry::class, ['id' => 'country_id']);
    }

    /**
     * @return string the name of the table
     */
    public static function tableName()
    {
        return 'user';
    }


    /**
     * Номер телефона пользователя
     *
     * @return string
     */
    public function getPhone()
    {
        return '+'.$this->phone_country_code.'-('.$this->phone_operator.')-'.$this->phone_number;
    }
}
