<?php

use yii\db\Migration;

class m161116_134616_users_table extends Migration
{

    public function safeUp()
    {

        $this->createTable('user', [
            'id'                 => 'pk',
            'country_id'         => 'int8 NOT NULL',
            'name'               => 'text NOT NULL',
            'phone_country_code' => 'varchar(8)',
            'phone_operator'     => 'varchar(8)',
            'phone_number'       => 'varchar(15)',
        ]);

        $this->createIndex('name_order_idx', 'user', 'name');

    }

    public function safeDown()
    {
        $this->dropIndex('name_order_idx', 'user');
        $this->dropTable('user');
        return true;
    }
}
