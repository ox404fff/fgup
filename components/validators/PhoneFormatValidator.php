<?php

namespace app\components\validators;

/**
 * Check is country id exists in database
 */
class PhoneFormatValidator extends CountryValidator
{

    /**
     * @var string country id attribute name
     */
    public $countryIdAttribute;

    /**
     * @var string phoneCountryCodeAttribute attribute name
     */
    public $phoneCountryCodeAttribute;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }


    /**
     * Check is country id exists in database
     *
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        if (!$this->isCountryExists($model->{$this->countryIdAttribute})) {
            return $this->addError(
                $model, $this->countryIdAttribute,
                \Yii::t('app', 'Что - то сломалось, попробуйте перезагрузить старницу.')
            );
        }

        $country = $this->getCountry($model->{$this->countryIdAttribute});

        if (!preg_match($country->phone_mask, $model->{$attribute})) {
            return $this->addError(
                $model, $attribute,
                \Yii::t('app', 'Телефон должен быть указан в формате '.$country->phone_placeholder)
            );
        }

        return null;
    }

}