<?php

namespace app\components\helpers;

class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * Make a string's first character uppercase
     * Works with various encodings
     * @param string $str
     * @param bool $lowerEnd
     * @param string $encoding
     * @return string
     */
    public static function ucFirst($str, $lowerEnd = false, $encoding = 'UTF-8')
    {
        $firstLetter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        if ($lowerEnd) {
            $strEnd = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            $strEnd = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        return $firstLetter.$strEnd;
    }
}